class ApiController < ApplicationController
  def show
    iban = Ibanomat.find :bank_code => params[:blz], :bank_account_number => params[:konto]
    render :json => iban
  end
end